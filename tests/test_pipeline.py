import pytest
import pandas as pd
import sys
from os import path

path_to_src = path.join(sys.path[0], '..', 'src')
sys.path.append(path_to_src)

from pipeline import process_data

@pytest.fixture
def input_data():
    # Setup code to create input data, to be reused
    data = {'value': [1, 2, 3, 4, 5]}
    return pd.DataFrame(data)

def test_process_data_size(input_data):
    # Tests the process_data function — structure and shape
    processed_data = process_data(input_data)
    assert 'result' in processed_data.columns
    assert processed_data.shape[0] == input_data.shape[0]
    
def test_process_data_calculation(input_data):
    # Tests the process_data function — the calculation
    processed_data = process_data(input_data)
    expected_output = pd.Series([2, 4, 6, 8, 10])
    assert all(processed_data['result'] == expected_output)
